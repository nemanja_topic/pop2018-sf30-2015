﻿using ConsoleApp.Model;
using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPFApp.Model;
using WPFApp.WPF;

namespace WPFApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            Aplikacija.Instance.UcitajLetove();

        } 

        private void BtnLogIn_Click(object sender, RoutedEventArgs e)
        {
            string username = TbUsername.Text;
            string password = TbPassword.Password;

            
                Aplikacija.Instance.UlogovaniKorisnik = PostojiKorisnik(username, password);

                if (Aplikacija.Instance.UlogovaniKorisnik == null)
                {
                    MessageBox.Show("Login podaci su neispravni!");
                }
                else
                {
                    UspesnoLogovanje();
                }
        }

        private void UspesnoLogovanje()
        {
            LoggedInWindow liw = new LoggedInWindow(Aplikacija.Instance.UlogovaniKorisnik);
            liw.Show();
            TbUsername.Clear();
            TbPassword.Clear();
        }

        public static Korisnik PostojiKorisnik(String username, String password)
        {
            foreach (var korisnik in Aplikacija.Instance.Korisnici)
            {
                if (korisnik.KorisnickoIme == username && korisnik.Lozinka == password)
                {
                    return korisnik;
                }
            }
            return null;
        }

        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            KorisniciEditWindow kew = new KorisniciEditWindow(new Korisnik(), KorisniciEditWindow.Option.REGISTER);
            if (kew.ShowDialog() == true)
            {
                MessageBox.Show("Uspesno ste ste registrovali!");
            }
        }

        private void BtnLetovi_Click_1(object sender, RoutedEventArgs e)
        {
            LetoviWindow lw = new LetoviWindow(new Aviokompanija(), LetoviWindow.Option.PUTNIK);
            lw.Show();
        }
    }
}
