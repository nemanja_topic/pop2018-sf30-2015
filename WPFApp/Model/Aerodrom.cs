﻿using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.Model
{
    public class Aerodrom : INotifyPropertyChanged, ICloneable
    {
        //public String Sifra { get; set; }
        //public String Naziv { get; set; }
        //public String Grad { get; set; }
        //public Boolean Obrisano { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public int Id { get; set; }
        private string sifra;
        private string naziv;
        private string grad;
        private bool obrisano;


        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; OnPropertyChanged("Sifra"); }
        }

        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; OnPropertyChanged("Naziv"); }
        }

        public string Grad
        {
            get { return grad; }
            set { grad = value; OnPropertyChanged("Grad"); }
        }

        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; OnPropertyChanged("Obrisano"); }
        }


        public Aerodrom()
        {
        }

        public Aerodrom(string sifra, string naziv, string grad, bool obrisano)
        {
            Sifra = sifra;
            Naziv = naziv;
            Grad = grad;
            Obrisano = obrisano;
        }

        public override string ToString()
        {
            return $"Sifra: {Sifra}, Naziv: {Naziv}, Grad: {Grad}";
        }

        public object Clone()
        {
            Aerodrom novi = new Aerodrom
            {
                Id = this.Id,
                Sifra = this.Sifra,
                Naziv = this.Naziv,
                Grad = this.Grad,
                Obrisano = this.Obrisano
            };

            return novi;

        }

        public void Dodavanje()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Aerodrom(sifra, naziv, grad, obrisano)" + " VALUES (@sifra, @naziv, @grad, @obrisano)";

                command.Parameters.Add(new SqlParameter("@sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@naziv", this.Naziv));
                command.Parameters.Add(new SqlParameter("@grad", this.Grad));
                command.Parameters.Add(new SqlParameter("@obrisano", false));
                command.ExecuteNonQuery();
            }
            Aplikacija.Instance.UcitajAerodrome();
        }

        public void Izmena()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"Update Aerodrom set sifra=@sifra, naziv=@naziv, grad=@grad, obrisano = @obrisano where id=@id";

                command.Parameters.Add(new SqlParameter("@id", this.Id));
                command.Parameters.Add(new SqlParameter("@sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@naziv", this.Naziv));
                command.Parameters.Add(new SqlParameter("@grad", this.Grad));
                command.Parameters.Add(new SqlParameter("@obrisano", this.Obrisano));
                command.ExecuteNonQuery();
            }
        }
    }
}
