﻿using ConsoleApp.Model;
using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFApp.Model
{
    public class Aviokompanija: INotifyPropertyChanged, ICloneable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public int Id { get; set; }
        private string sifraAviokompanije;
        private ObservableCollection<Let> letovi;
        private bool obrisano;

        public string SifraAviokompanije
        {
            get { return sifraAviokompanije; }
            set { sifraAviokompanije = value; }
        }

        public ObservableCollection<Let> Letovi
        {
            get { return letovi; }
            set { letovi = value; }
        }

        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; }
        }

        public Aviokompanija()
        {
        }

        public Aviokompanija(string sifraAviokompanije, ObservableCollection<Let> letovi, bool obrisano)
        {
            SifraAviokompanije = sifraAviokompanije;
            Letovi = letovi;
            Obrisano = obrisano;
        }

        public object Clone()
        {
            Aviokompanija nova = new Aviokompanija
            {
                Id = this.Id,
                SifraAviokompanije = this.SifraAviokompanije,
                Letovi = this.Letovi,
                Obrisano = this.Obrisano
            };
            return nova;
        }

        public void Dodavanje()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Aviokompanija(sifraAviokompanije, obrisano)" + " VALUES (@sifraAviokompanije, @obrisano)";

                command.Parameters.Add(new SqlParameter("@sifraAviokompanije", this.SifraAviokompanije));
                command.Parameters.Add(new SqlParameter("@obrisano", false));
                command.ExecuteNonQuery();
            }
            Aplikacija.Instance.UcitajAviokompanije();
        }

        public void Izmena()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"Update Aviokompanija set sifraAviokompanije=@sifraAviokompanije, obrisano = @obrisano where id=@id";

                command.Parameters.Add(new SqlParameter("@id", this.Id));
                command.Parameters.Add(new SqlParameter("@sifraAviokompanije", this.SifraAviokompanije));
                command.Parameters.Add(new SqlParameter("@obrisano", this.Obrisano));
                command.ExecuteNonQuery();
            }
        }

        public override string ToString()
        {
            return sifraAviokompanije.ToString();
        }
    }
}
