﻿using ConsoleApp.Model;
using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFApp.Model
{
    public class Avion : INotifyPropertyChanged, ICloneable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public int Id { get; set; }
        private string pilot;
        private int idLeta;
        private Let let;
        private ObservableCollection<Sediste> sedista;
        private int idAviokompanije;
        private Aviokompanija aviokompanija;

        private bool obrisano;

        public string Pilot
        {
            get { return pilot; }
            set { pilot = value; OnPropertyChanged("Pilot"); }
        }

        public int IdLeta
        {
            get { return idLeta; }
            set { idLeta = value; OnPropertyChanged("IdLeta"); }
        }

        public static Let GetLetById(int id)
        {
            foreach (var let in Aplikacija.Instance.Letovi)
            {
                if (let.Id == id)
                {
                    return let;
                }
            }
            return null;
        }

        public Let Let
        {
            get {
                if (let == null)
                {
                    let = GetLetById(IdLeta);
                }
                return let; }
            set {
                 let = value;
                 IdLeta = let.Id;
                 OnPropertyChanged("Let");
            }
        }

        public ObservableCollection<Sediste> Sedista
        {
            get { return sedista; }
            set { sedista = value; OnPropertyChanged("Sedista"); }
        }

        public int IdAviokompanije
        {
            get { return idAviokompanije; }
            set { idAviokompanije = value; OnPropertyChanged("IdAviokompanije"); }
        }

        public static Aviokompanija GetAviokompanijaById(int id)
        {
            foreach (var aviokompanija in Aplikacija.Instance.Aviokompanije)
            {
                if (aviokompanija.Id == id)
                {
                    return aviokompanija;
                }
            }
            return null;
        }

        public Aviokompanija Aviokompanija
        {
            get {
                if (aviokompanija == null)
                {
                    aviokompanija = GetAviokompanijaById(IdAviokompanije);
                }
                return aviokompanija; }
            set { aviokompanija = value;
                IdAviokompanije = aviokompanija.Id;
                OnPropertyChanged("Aviokompanija"); }
        }

        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; OnPropertyChanged("Obrisano"); }
        }

        public Avion()
        {
        }

        public Avion(string pilot, int idLeta, Let let, ObservableCollection<Sediste> sedista, int idAviokompanije, Aviokompanija aviokompanija, bool obrisano)
        {
            Pilot = pilot;
            IdLeta = idLeta;
            Let = let;
            Sedista = sedista;
            IdAviokompanije = idAviokompanije;
            Aviokompanija = aviokompanija;
            Obrisano = obrisano;
        }

        public object Clone()
        {
            Avion novi = new Avion
            {
                Id = this.Id,
                Pilot = this.Pilot,
                IdLeta = this.IdLeta,
                Let = this.Let,
                Sedista = this.Sedista,
                IdAviokompanije = this.IdAviokompanije,
                Aviokompanija = this.Aviokompanija,
                Obrisano = this.Obrisano
            };
            return novi;
        }

        public void Dodavanje()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Avion(pilot, idLeta, idAviokompanije, obrisano)" + " VALUES (@pilot, @idLeta, @idAviokompanije, @obrisano)";

                command.Parameters.Add(new SqlParameter("@pilot", this.Pilot));
                command.Parameters.Add(new SqlParameter("@idLeta", this.IdLeta));
                command.Parameters.Add(new SqlParameter("@idAviokompanije", this.IdAviokompanije));
                command.Parameters.Add(new SqlParameter("@obrisano", false));
                command.ExecuteNonQuery();
            }
            Aplikacija.Instance.UcitajAvione();
        }

        public void Izmena()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"Update Avion set pilot=@pilot, idLeta=@idLeta, idAviokompanije=@idAviokompanije, obrisano = @obrisano where id=@id";

                command.Parameters.Add(new SqlParameter("@id", this.Id));
                command.Parameters.Add(new SqlParameter("@pilot", this.Pilot));
                command.Parameters.Add(new SqlParameter("@idLeta", this.idLeta));
                command.Parameters.Add(new SqlParameter("@idAviokompanije", this.idAviokompanije));
                command.Parameters.Add(new SqlParameter("@obrisano", this.Obrisano));
                command.ExecuteNonQuery();
            }
        }
    }
}
