﻿using ConsoleApp.Model;
using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFApp.Model
{
    public class Karta : INotifyPropertyChanged, ICloneable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public int Id { get; set; }
        private int idLeta;
        private Let let;
        private string nazivPutnika;
        private int idSedista;
        private Sediste sediste;
        private bool biznis;
        private int kapija;
        private double ukupnaCena;
        private bool obrisano;

        public int IdLeta
        {
            get { return idLeta; }
            set { idLeta = value; OnPropertyChanged("IdLeta"); }
        }

        public static Let GetLetById(int id)
        {
            foreach (var let in Aplikacija.Instance.Letovi)
            {
                if (let.Id == id)
                {
                    return let;
                }
            }
            return null;
        }

        public Let Let
        {
            get
            {
                if (let == null)
                {
                    let = GetLetById(IdLeta);
                }
                return let;
            }
            set { let = value; OnPropertyChanged("Let"); }
        }

        public string NazivPutnika
        {
            get { return nazivPutnika; }
            set { nazivPutnika = value; OnPropertyChanged("NazivPutnika"); }
        }

        public int IdSedista
        {
            get { return idSedista; }
            set { idSedista = value; OnPropertyChanged("IdSedista"); }
        }

        public static Sediste GetSedisteById(int id)
        {
            foreach (var sediste in Aplikacija.Instance.Sedista)
            {
                if (sediste.Id == id)
                {
                    return sediste;
                }
            }
            return null;
        }

        public Sediste Sediste
        {
            get
            {
                if (sediste == null)
                {
                    sediste = GetSedisteById(IdSedista);
                }
                return sediste;
            }
            set { sediste = value; OnPropertyChanged("Sediste"); }
        }

        public bool Biznis
        {
            get { return biznis; }
            set { biznis = value; OnPropertyChanged("Biznis"); }
        }

        public int Kapija
        {
            get { return kapija; }
            set { kapija = value; OnPropertyChanged("Kapija"); }
        }

        public double UkupnaCena
        {
            get { return ukupnaCena; }
            set { ukupnaCena = value; OnPropertyChanged("UkupnaCena"); }
        }

        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; OnPropertyChanged("Obrisano"); }
        }

        public Karta()
        {
        }

        public Karta(int idLeta, Let let, string nazivPutnika, int idSedista, Sediste sediste, bool biznis, int kapija, double ukupnaCena, bool obrisano)
        {
            this.idLeta = idLeta;
            this.let = let;
            this.nazivPutnika = nazivPutnika;
            this.idSedista = idSedista;
            this.sediste = sediste;
            this.biznis = biznis;
            this.kapija = kapija;
            this.ukupnaCena = ukupnaCena;
            this.obrisano = obrisano;
        }

        public object Clone()
        {
            Karta nova = new Karta
            {
                Id = this.Id,
                IdLeta = this.IdLeta,
                Let = this.Let,
                NazivPutnika = this.NazivPutnika,
                IdSedista = this.IdSedista,
                Sediste = this.Sediste,
                Biznis = this.Biznis,
                Kapija = this.Kapija,
                UkupnaCena = this.UkupnaCena,
                Obrisano = this.Obrisano
            };
            return nova;
        }

        public void Dodavanje()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Karta(idLeta, korisnickoIme, idSedista, biznis, kapija, ukupnaCena, obrisano)" + " VALUES (@idLeta, @korisnickoIme, @idSedista, @biznis, @kapija, @ukupnaCena, @obrisano)";

                command.Parameters.Add(new SqlParameter("@idLeta", this.IdLeta));
                command.Parameters.Add(new SqlParameter("@korisnickoIme", this.NazivPutnika));
                command.Parameters.Add(new SqlParameter("@idSedista", this.IdSedista));
                command.Parameters.Add(new SqlParameter("@biznis", this.Biznis));
                command.Parameters.Add(new SqlParameter("@kapija", this.Kapija));
                command.Parameters.Add(new SqlParameter("@ukupnaCena", this.UkupnaCena));
                command.Parameters.Add(new SqlParameter("@obrisano", false));
                command.ExecuteNonQuery();
            }
            Aplikacija.Instance.UcitajKarte();
        }

        public void Izmena()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"Update Karta set idLeta=@idLeta, korisnickoIme=@korisnickoIme, idSedista=@idSedista, biznis=@biznis, kapija=@kapija, ukupnaCena=@ukupnaCena, obrisano=@obrisano where id=@id";

                command.Parameters.Add(new SqlParameter("@id", this.Id));
                command.Parameters.Add(new SqlParameter("@idLeta", this.IdLeta));
                command.Parameters.Add(new SqlParameter("@korisnickoIme", this.NazivPutnika));
                command.Parameters.Add(new SqlParameter("@idSedista", this.IdSedista));
                command.Parameters.Add(new SqlParameter("@biznis", this.Biznis));
                command.Parameters.Add(new SqlParameter("@kapija", this.Kapija));
                command.Parameters.Add(new SqlParameter("@ukupnaCena", this.UkupnaCena));
                command.Parameters.Add(new SqlParameter("@obrisano", this.Obrisano));
                command.ExecuteNonQuery();
            }
        }
    }
}
