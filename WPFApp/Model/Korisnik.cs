﻿using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.Model
{
    public class Korisnik : INotifyPropertyChanged, ICloneable
    {
        //public String Ime { get; set; }
        //public String Prezime { get; set; }
        //public String AdresaStanovanja { get; set; }
        //public EPol Pol { get; set; }
        //public String KorisnickoIme { get; set; }
        //public String Lozinka { get; set; }
        //public ETipKorisnika TipKorisnika { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public int Id { get; set; }
        private string ime;
        private string prezime;
        private string email;
        private string adresaStanovanja;
        private EPol pol;
        private string korisnickoIme;
        private string lozinka;
        private ETipKorisnika tipKorisnika;
        private bool obrisano;

        public string Ime
        {
            get { return ime; }
            set { ime = value; OnPropertyChanged("Ime"); }
        }

        public string Prezime
        {
            get { return prezime; }
            set { prezime = value; OnPropertyChanged("Prezime"); }
        }

        public string Email
        {
            get { return email; }
            set { email = value; OnPropertyChanged("Email"); }
        }

        public string AdresaStanovanja
        {
            get { return adresaStanovanja; }
            set { adresaStanovanja = value; OnPropertyChanged("AdresaStanovanja"); }
        }

        public EPol Pol
        {
            get { return pol; }
            set { pol = value; OnPropertyChanged("Pol"); }
        }

        public string KorisnickoIme
        {
            get { return korisnickoIme; }
            set { korisnickoIme = value; OnPropertyChanged("KorisnickoIme"); }
        }

        public string Lozinka
        {
            get { return lozinka; }
            set { lozinka = value; OnPropertyChanged("Lozinka"); }
        }

        public ETipKorisnika TipKorisnika
        {
            get { return tipKorisnika; }
            set { tipKorisnika = value; OnPropertyChanged("TipKorisnika"); }
        }

        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; OnPropertyChanged("Obrisano"); }
        }


        public override string ToString()
        {
            //return "Ime" + Ime;
            return $"Ime: {Ime}, Prezime: {Prezime}, Email: {Email}, Adresa stanovanja: {AdresaStanovanja}, Pol: {Pol}, Tip Korisnika: {TipKorisnika}";
        }

        public Korisnik()
        {
        }

        public Korisnik(String ime, String prezime, String email, String adresaStanovanja, EPol pol, String korisnickoIme, String lozinka, ETipKorisnika tipKorisnika, Boolean obrisano)
        {
            this.Ime = ime;
            this.Prezime = prezime;
            this.Email = email;
            this.AdresaStanovanja = adresaStanovanja;
            this.Pol = pol;
            this.KorisnickoIme = korisnickoIme;
            this.Lozinka = lozinka;
            this.TipKorisnika = tipKorisnika;
            this.Obrisano = obrisano;
        }

        public object Clone()
        {
            Korisnik novi = new Korisnik
            {
                Id = this.Id,
                Ime = this.Ime,
                Prezime = this.Prezime,
                Email = this.Email,
                AdresaStanovanja = this.AdresaStanovanja,
                Pol = this.Pol,
                KorisnickoIme = this.KorisnickoIme,
                Lozinka = this.Lozinka,
                TipKorisnika = this.TipKorisnika,
                Obrisano = this.Obrisano
            };

            return novi;
        }

        public void Dodavanje()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Korisnik(ime, prezime, email, adresaStanovanja, pol, korisnickoIme, lozinka, tipKorisnika, obrisano)" + " VALUES (@ime, @prezime, @email, @adresaStanovanja, @pol, @korisnickoIme, @lozinka, @tipKorisnika, @obrisano)";

                command.Parameters.Add(new SqlParameter("@ime", this.Ime));
                command.Parameters.Add(new SqlParameter("@prezime", this.Prezime));
                command.Parameters.Add(new SqlParameter("@email", this.Email));
                command.Parameters.Add(new SqlParameter("@adresaStanovanja", this.AdresaStanovanja));
                command.Parameters.Add(new SqlParameter("@pol", this.Pol.ToString()));
                command.Parameters.Add(new SqlParameter("@korisnickoIme", this.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@lozinka", this.Lozinka));
                command.Parameters.Add(new SqlParameter("@tipKorisnika", this.TipKorisnika.ToString()));
                command.Parameters.Add(new SqlParameter("@obrisano", false));
                command.ExecuteNonQuery();
            }
            Aplikacija.Instance.UcitajKorisnike();
        }

        public void Izmena()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"Update Korisnik set ime=@ime, prezime=@prezime, email=@email, adresaStanovanja=@adresaStanovanja, pol=@pol, korisnickoIme=@korisnickoIme, lozinka=@lozinka, tipKorisnika=@tipKorisnika, obrisano=@obrisano where id=@id";

                command.Parameters.Add(new SqlParameter("@id", this.Id));
                command.Parameters.Add(new SqlParameter("@ime", this.Ime));
                command.Parameters.Add(new SqlParameter("@prezime", this.Prezime));
                command.Parameters.Add(new SqlParameter("@email", this.Email));
                command.Parameters.Add(new SqlParameter("@adresaStanovanja", this.AdresaStanovanja));
                command.Parameters.Add(new SqlParameter("@pol", this.Pol.ToString()));
                command.Parameters.Add(new SqlParameter("@korisnickoIme", this.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@lozinka", this.Lozinka));
                command.Parameters.Add(new SqlParameter("@tipKorisnika", this.TipKorisnika.ToString()));
                command.Parameters.Add(new SqlParameter("@obrisano", this.Obrisano));
                command.ExecuteNonQuery();
            }
        }
    }
}
