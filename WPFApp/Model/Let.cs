﻿using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFApp.Model;

namespace ConsoleApp.Model
{
    public class Let : INotifyPropertyChanged, ICloneable
    {
        //public String BrojLeta { get; set; }
        //public String Odrediste { get; set; }
        //public String Destinacija { get; set; }
        //public DateTime VremePolaska { get; set; }
        //public DateTime VremeDolaska { get; set; }
        //public Double CenaLeta { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public int Id { get; set; }
        private string sifraLeta;
        private string polaziste;
        private string destinacija;
        private DateTime vremePolaska;
        private DateTime vremeDolaska;
        private double cenaKarte;
        private int idAviokompanije;
        private Aviokompanija aviokompanija;
        private bool obrisano;

        public string SifraLeta
        {
            get { return sifraLeta; }
            set { sifraLeta = value; OnPropertyChanged("SifraLeta"); }
        }

        public string Polaziste
        {
            get { return polaziste; }
            set { polaziste = value; OnPropertyChanged("Polaziste"); }
        }

        public string Destinacija
        {
            get { return destinacija; }
            set { destinacija = value; OnPropertyChanged("Destinacija"); }
        }

        public DateTime VremePolaska
        {
            get { return vremePolaska; }
            set { vremePolaska = value; OnPropertyChanged("VremePolaska"); }
        }

        public DateTime VremeDolaska
        {
            get { return vremeDolaska; }
            set { vremeDolaska = value; OnPropertyChanged("VremeDolaska"); }
        }

        public double CenaKarte
        {
            get { return cenaKarte; }
            set { cenaKarte = value; OnPropertyChanged("CenaKarte"); }
        }

        public int IdAviokompanije
        {
            get { return idAviokompanije; }
            set { idAviokompanije = value; OnPropertyChanged("IdAviokompanije"); }
        }

        public static Aviokompanija GetAviokompanijaById(int id)
        {
            foreach (var aviokompanija in Aplikacija.Instance.Aviokompanije)
            {
                if (aviokompanija.Id == id)
                {
                    return aviokompanija;
                }
            }
            return null;
        }

        public Aviokompanija Aviokompanija
        {
            get
            {
                if (aviokompanija == null)
                {
                    aviokompanija = GetAviokompanijaById(IdAviokompanije);
                }
                return aviokompanija;
            }
            set { aviokompanija = value;
                IdAviokompanije = aviokompanija.Id; OnPropertyChanged("Aviokompanija"); }
        }

        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; OnPropertyChanged("Obrisano"); }
        }

        public override string ToString()
        {
            return sifraLeta.ToString();
        }

        public Let()
        {
        }

        public Let(string sifraLeta, string polaziste, string destinacija, DateTime vremePolaska, DateTime vremeDolaska, int idAviokompanije, Aviokompanija aviokompanija, double cenaKarte, bool obrisano)
        {
            SifraLeta = sifraLeta;
            Polaziste = polaziste;
            Destinacija = destinacija;
            VremePolaska = vremePolaska;
            VremeDolaska = vremeDolaska;
            IdAviokompanije = idAviokompanije;
            Aviokompanija = aviokompanija;
            CenaKarte = cenaKarte;
            Obrisano = obrisano;
        }

        public object Clone()
        {
            return new Let
            {
                Id = this.Id,
                SifraLeta = this.SifraLeta,
                Polaziste = this.Polaziste,
                Destinacija = this.Destinacija,
                VremeDolaska = this.VremeDolaska,
                VremePolaska = this.VremePolaska,
                IdAviokompanije = this.IdAviokompanije,
                Aviokompanija = this.Aviokompanija,
                CenaKarte = this.CenaKarte,
                Obrisano = this.Obrisano
            };
        }

        public void Dodavanje()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Let(sifraLeta, polaziste, destinacija, vremePolaska, vremeDolaska, cenaKarte, idAviokompanije, obrisano)" + " VALUES (@sifraLeta, @polaziste, @destinacija, @vremePolaska, @vremeDolaska, @cenaKarte, @idAviokompanije, @obrisano)";

                command.Parameters.Add(new SqlParameter("@sifraLeta", this.SifraLeta));
                command.Parameters.Add(new SqlParameter("@polaziste", this.Polaziste));
                command.Parameters.Add(new SqlParameter("@destinacija", this.Destinacija));
                command.Parameters.Add(new SqlParameter("@vremePolaska", this.VremePolaska));
                command.Parameters.Add(new SqlParameter("@vremeDolaska", this.VremeDolaska));
                command.Parameters.Add(new SqlParameter("@cenaKarte", this.CenaKarte));
                command.Parameters.Add(new SqlParameter("@idAviokompanije", this.IdAviokompanije));
                command.Parameters.Add(new SqlParameter("@obrisano", false));
                command.ExecuteNonQuery();
            }
            Aplikacija.Instance.UcitajLetove();
        }

        public void Izmena()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"Update Let set sifraLeta=@sifraLeta, polaziste=@polaziste, destinacija=@destinacija, vremePolaska=@vremePolaska, vremeDolaska=@vremeDolaska, cenaKarte=@cenaKarte, idAviokompanije=@idAviokompanije, obrisano=@obrisano where id=@id";

                command.Parameters.Add(new SqlParameter("@id", this.Id));
                command.Parameters.Add(new SqlParameter("@sifraLeta", this.SifraLeta));
                command.Parameters.Add(new SqlParameter("@polaziste", this.Polaziste));
                command.Parameters.Add(new SqlParameter("@destinacija", this.Destinacija));
                command.Parameters.Add(new SqlParameter("@vremePolaska", this.VremePolaska));
                command.Parameters.Add(new SqlParameter("@vremeDolaska", this.VremeDolaska));
                command.Parameters.Add(new SqlParameter("@cenaKarte", this.CenaKarte));
                command.Parameters.Add(new SqlParameter("@idAviokompanije", this.IdAviokompanije));
                command.Parameters.Add(new SqlParameter("@obrisano", this.Obrisano));
                command.ExecuteNonQuery();
            }
        }
    }
}
