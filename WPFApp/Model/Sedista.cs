﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFApp.Model
{
    public class Sedista: INotifyPropertyChanged, ICloneable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public int Id { get; set; }
        private ObservableCollection<Sediste> slobodnaSedista;
        private ObservableCollection<Sediste> zauzetaSedista;
        private bool obrisano;
        private int idAviona;

        public ObservableCollection<Sediste> SlobodnaSedista
        {
            get { return slobodnaSedista; }
            set { slobodnaSedista = value; OnPropertyChanged("ZauzetaSedista"); }
        }

        public ObservableCollection<Sediste> ZauzetaSedista
        {
            get { return zauzetaSedista; }
            set { zauzetaSedista = value; OnPropertyChanged("ZauzetaSedista"); }
        }

        public bool Obrisano
        {
            get { return obrisano; }
            set { obrisano = value; OnPropertyChanged("Obrisano"); }
        }

        public int IdAviona
        {
            get { return idAviona; }
            set { idAviona = value; OnPropertyChanged("IdAviona"); }
        }

        public Sedista()
        {
        }

        public Sedista(ObservableCollection<Sediste> slobodnaSedista, ObservableCollection<Sediste> zauzetaSedista, bool obrisano)
        {
            this.slobodnaSedista = slobodnaSedista;
            this.zauzetaSedista = zauzetaSedista;
            this.obrisano = obrisano;
        }

        public object Clone()
        {
            Sedista sedista = new Sedista
            {
                Id = this.Id,
                SlobodnaSedista = this.SlobodnaSedista,
                ZauzetaSedista = this.ZauzetaSedista,
                Obrisano = this.Obrisano,
                IdAviona = this.IdAviona
            };
            return sedista;
        }
    }
}