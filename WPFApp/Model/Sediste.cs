﻿
using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFApp.Model
{
    public class Sediste: INotifyPropertyChanged, ICloneable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public int Id { get; set; }
        private int kolona;
        private int red;
        private bool biznis;
        private bool zauzeto;
        private int avionId;
        private Avion avion;

        public int Kolona
        {
            get { return kolona; }
            set { kolona = value; OnPropertyChanged("Kolona"); }
        }

        public int Red
        {
            get { return red; }
            set { red = value; OnPropertyChanged("Red"); }
        }

        public bool Biznis
        {
            get { return biznis; }
            set { biznis = value; OnPropertyChanged("Biznis"); }
        }

        public bool Zauzeto
        {
            get { return zauzeto; }
            set { zauzeto = value; OnPropertyChanged("Zauzeto"); }
        }

        public int AvionId
        {
            get { return avionId; }
            set { avionId = value; OnPropertyChanged("AvionId"); }
        }

        public static Avion GetById(int id)
        {
            foreach (var avion in Aplikacija.Instance.Avioni)
            {
                if (avion.Id == id)
                {
                    return avion;
                }
            }
            return null;
        }

        public Avion Avion
        {
            get {
                if (avion == null)
                {
                    avion = GetById(AvionId);
                }
                return avion;
            }
            set { avion = value; OnPropertyChanged("Avion"); }
        }

        public Sediste()
        {
        }

        public override string ToString()
        {
            if(biznis == true)
            {
                return "Biznis " + "K: " + kolona.ToString() + " " + "R: " + red.ToString();
            }
            else
            {
                return "Ekonomska " + "K: " + kolona.ToString() + " " + "R: " + red.ToString();
            }
        }

        public Sediste(int id, int kolona, int red, bool biznis, bool zauzeto, int avionId, Avion avion)
        {
            Id = id;
            this.kolona = kolona;
            this.red = red;
            this.biznis = biznis;
            this.zauzeto = zauzeto;
            this.avionId = avionId;
            this.avion = avion;
        }

        public object Clone()
        {
            Sediste novo = new Sediste
            {
                Id = this.Id,
                Kolona = this.Kolona,
                Red = this.Red,
                Biznis = this.Biznis,
                Zauzeto = this.Zauzeto,
                AvionId = this.AvionId,
                Avion = this.Avion
            };
            return novo;
        }

        public void Dodavanje()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Sediste(kolona, red, biznis, zauzeto, avionId)" + " VALUES (@kolona, @red, @biznis, @zauzeto, @avionId)";

                command.Parameters.Add(new SqlParameter("@kolona", this.Kolona));
                command.Parameters.Add(new SqlParameter("@red", this.Red));
                command.Parameters.Add(new SqlParameter("@biznis", this.Biznis));
                command.Parameters.Add(new SqlParameter("@zauzeto", false));
                command.Parameters.Add(new SqlParameter("@avionId", this.AvionId));
                command.ExecuteNonQuery();
            }
            Aplikacija.Instance.UcitajSedista();
        }

        public void Izmena()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Aplikacija.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"Update Sediste set kolona=@kolona, red=@red, biznis=@biznis, zauzeto=@zauzeto, avionId=@avionId where id=@id";

                command.Parameters.Add(new SqlParameter("@id", this.Id));
                command.Parameters.Add(new SqlParameter("@kolona", this.Kolona));
                command.Parameters.Add(new SqlParameter("@red", this.Red));
                command.Parameters.Add(new SqlParameter("@biznis", this.Biznis));
                command.Parameters.Add(new SqlParameter("@zauzeto", this.Zauzeto));
                command.Parameters.Add(new SqlParameter("@avionId", this.AvionId));
                command.ExecuteNonQuery();
            }
        }
    }
}
