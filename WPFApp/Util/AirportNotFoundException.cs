﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.Util
{
    class AirportNotFoundException : Exception
    {
        public AirportNotFoundException() : base()
        {

        }

        public AirportNotFoundException(String message) : base(message)
        {

        }

        public AirportNotFoundException(String message, Exception exception): base(message, exception)
        {

        }
    }
}
