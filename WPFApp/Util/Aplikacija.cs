﻿using ConsoleApp.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WPFApp.Model;

namespace ConsoleApp.Util
{
    class Aplikacija
    {
        //lista ne implementira INotifyPropertyChanged, a ObservableCollection implementira
        public ObservableCollection<Aerodrom> Aerodromi { get; set; }
        public ObservableCollection<Let> Letovi { get; set; }
        public ObservableCollection<Korisnik> Korisnici { get; set; }
        public ObservableCollection<Sediste> Sedista { get; set; }
        public ObservableCollection<Aviokompanija> Aviokompanije { get; set; }
        public ObservableCollection<Avion> Avioni { get; set; }
        public ObservableCollection<Karta> Karte { get; set; }

        public Korisnik UlogovaniKorisnik;

        public const String CONNECTION_STRING = @"Server=(localdb)\MSSQLLocalDB;
                                                    Initial Catalog=WPFApp;
                                                    Trusted_Connection=True;";

        private Aplikacija()
        {
            UlogovaniKorisnik = null;

            Korisnici = new ObservableCollection<Korisnik>();
            Aerodromi = new ObservableCollection<Aerodrom>();
            Letovi = new ObservableCollection<Let>();
            Sedista = new ObservableCollection<Sediste>();
            Aviokompanije = new ObservableCollection<Aviokompanija>();
            Avioni = new ObservableCollection<Avion>();
            Karte = new ObservableCollection<Karta>();

            UcitajSedista();
            UcitajAerodrome();
            UcitajLetove();
            UcitajKorisnike();
            UcitajAviokompanije();
            UcitajAvione();
            UcitajKarte();
        }

        public void UcitajKarte()
        {
            Karte.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"Select * from Karta";

                SqlDataAdapter daKarte = new SqlDataAdapter();
                DataSet dsKarte = new DataSet();

                daKarte.SelectCommand = command;

                daKarte.Fill(dsKarte, "Karta");

                foreach (DataRow row in dsKarte.Tables["Karta"].Rows)
                {
                    Karta karta = new Karta();

                    karta.Id = (int)row["id"];
                    karta.IdLeta = (int)row["idLeta"];
                    karta.NazivPutnika = (string)row["korisnickoIme"];
                    karta.IdSedista = (int)row["idSedista"];
                    karta.Biznis = (bool)row["biznis"];
                    karta.Kapija = (int)row["kapija"];
                    karta.UkupnaCena = (double)row["ukupnaCena"];
                    karta.Obrisano = (bool)row["obrisano"];

                    Karte.Add(karta);
                }
            }
        }

        public void UcitajAvione()
        {
            Avioni.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"Select * from Avion";

                SqlDataAdapter daAvioni = new SqlDataAdapter();
                DataSet dsAvioni = new DataSet();

                daAvioni.SelectCommand = command;

                daAvioni.Fill(dsAvioni, "Avion");

                foreach (DataRow row in dsAvioni.Tables["Avion"].Rows)
                {
                    Avion avion = new Avion();

                    avion.Id = (int)row["id"];
                    avion.Pilot = (string)row["pilot"];
                    avion.IdLeta = (int)row["idLeta"];
                    avion.IdAviokompanije = (int)row["idAviokompanije"];
                    avion.Obrisano = (bool)row["obrisano"];

                    Avioni.Add(avion);
                }
            }
        }

        public void UcitajAviokompanije()
        {
            Aviokompanije.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"Select * from Aviokompanija";

                SqlDataAdapter daAviokompanija = new SqlDataAdapter();
                DataSet dsAviokompanija = new DataSet();

                daAviokompanija.SelectCommand = command;

                daAviokompanija.Fill(dsAviokompanija, "Aviokompanija");

                foreach (DataRow row in dsAviokompanija.Tables["Aviokompanija"].Rows)
                {
                    Aviokompanija aviokompanija = new Aviokompanija();

                    aviokompanija.Id = (int)row["id"];
                    aviokompanija.SifraAviokompanije = (string)row["sifraAviokompanije"];
                    aviokompanija.Obrisano = (bool)row["obrisano"];

                    Aviokompanije.Add(aviokompanija);
                }
            }
        }

        public void UcitajKorisnike()
        {
            Korisnici.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"Select * from Korisnik";

                SqlDataAdapter daKorisnici = new SqlDataAdapter();
                DataSet dsKorisnici = new DataSet();

                daKorisnici.SelectCommand = command;

                daKorisnici.Fill(dsKorisnici, "Korisnik");

                foreach (DataRow row in dsKorisnici.Tables["Korisnik"].Rows)
                {
                    Korisnik korisnik = new Korisnik();

                    korisnik.Id = (int)row["id"];
                    korisnik.Ime = (string)row["ime"];
                    korisnik.Prezime = (string)row["prezime"];
                    korisnik.Email = (string)row["email"];
                    korisnik.AdresaStanovanja = (string)row["adresaStanovanja"];
                    korisnik.KorisnickoIme = (string)row["korisnickoIme"];
                    korisnik.Lozinka = (string)row["lozinka"];
                    korisnik.Obrisano = (bool)row["obrisano"];

                    if (row["tipKorisnika"].ToString() == ETipKorisnika.ADMINISTRATOR.ToString())
                    {
                        korisnik.TipKorisnika = ETipKorisnika.ADMINISTRATOR;
                    }
                    if (row["tipKorisnika"].ToString() == ETipKorisnika.PUTNIK.ToString())
                    {
                        korisnik.TipKorisnika = ETipKorisnika.PUTNIK;
                    }
                    if (row["tipKorisnika"].ToString() == ETipKorisnika.NEREGISTROVAN.ToString())
                    {
                        korisnik.TipKorisnika = ETipKorisnika.NEREGISTROVAN;
                    }

                    if (row["pol"].ToString() == EPol.M.ToString())
                    {
                        korisnik.Pol = EPol.M;
                    }
                    if (row["pol"].ToString() == EPol.Z.ToString())
                    {
                        korisnik.Pol = EPol.Z;
                    }

                    Korisnici.Add(korisnik);
            
                }
            }
        }

        public void UcitajLetove()
        {
            Letovi.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"Select * from Let";

                SqlDataAdapter daLetovi = new SqlDataAdapter();
                DataSet dsLetovi = new DataSet();

                daLetovi.SelectCommand = command;

                daLetovi.Fill(dsLetovi, "Let");

                foreach (DataRow row in dsLetovi.Tables["Let"].Rows)
                {
                    Let let = new Let();

                    let.Id = (int)row["id"];
                    let.SifraLeta = (string)row["sifraLeta"];
                    let.Polaziste = (string)row["polaziste"];
                    let.Destinacija = (string)row["destinacija"];
                    let.VremePolaska = (DateTime)row["vremePolaska"];
                    let.VremeDolaska = (DateTime)row["vremeDolaska"];
                    let.CenaKarte = (double)row["cenaKarte"];
                    let.IdAviokompanije = (int)row["idAviokompanije"];
                    let.Obrisano = (bool)row["obrisano"];

                    Letovi.Add(let);
                }
            }
        }

        public void UcitajSedista()
        {
            Sedista.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"Select * from Sediste";

                SqlDataAdapter daSedista = new SqlDataAdapter();
                DataSet dsSedista = new DataSet();

                daSedista.SelectCommand = command;

                daSedista.Fill(dsSedista, "Sediste");

                foreach (DataRow row in dsSedista.Tables["Sediste"].Rows)
                {
                    Sediste sediste = new Sediste();

                    sediste.Id = (int)row["id"];
                    sediste.Kolona = (int)row["kolona"];
                    sediste.Red = (int)row["red"];
                    sediste.Biznis = (bool)row["biznis"];
                    sediste.Zauzeto = (bool)row["zauzeto"];
                    sediste.AvionId = (int)row["avionId"];

                    Sedista.Add(sediste);
                }
            }
        }

        public void UcitajAerodrome()
        {
            Aerodromi.Clear();
            using  (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"Select * from Aerodrom";

                SqlDataAdapter daAerodromi = new SqlDataAdapter();
                DataSet dsAerodromi = new DataSet();

                daAerodromi.SelectCommand = command;

                daAerodromi.Fill(dsAerodromi, "Aerodrom");

                foreach(DataRow row in dsAerodromi.Tables["Aerodrom"].Rows)
                {
                    Aerodrom aerodrom = new Aerodrom();

                    aerodrom.Id = (int)row["id"];
                    aerodrom.Sifra = (string)row["sifra"];
                    aerodrom.Naziv = (string)row["naziv"];
                    aerodrom.Grad = (string)row["grad"];
                    aerodrom.Obrisano = (bool)row["obrisano"];

                    Aerodromi.Add(aerodrom);
                }
            }
        }



    /*    private void UcitajSveKarte()
        {
            var karta1 = new Karta
            {
                Id = 1,
                SifraLeta = "123",
                NazivPutnika = "Pera Peric",
                BrojSedista = 24,
                Biznis = false,
                Kapija = 150,
                UkupnaCena = 25350,
                Obrisano = false
            };
            var karta2 = new Karta
            {
                Id = 2,
                SifraLeta = "124",
                NazivPutnika = "Mika Mikic",
                BrojSedista = 25,
                Biznis = true,
                Kapija = 151,
                UkupnaCena = 55242,
                Obrisano = false
            };
            Karte.Add(karta1);
            Karte.Add(karta2);
        }

        private void UcitajSveAvione()
        {
            var avion1 = new Avion
            {
                Id = 1,
                Pilot = "Pera Peric",
                SifraLeta = "123",
                BiznisKlasa = new Sedista(),
                EkonomskaKlasa = new Sedista(),
                SifraAviokompanije = "ABC",
                Obrisano = false
            };
            var avion2 = new Avion
            {
                Id = 2,
                Pilot = "Mika Mikic",
                SifraLeta = "124",
                BiznisKlasa = new Sedista(),
                EkonomskaKlasa = new Sedista(),
                SifraAviokompanije = "ABC",
                Obrisano = false
            };
            Avioni.Add(avion1);
            Avioni.Add(avion2);
        }

        private void UcitajSveAviokompanije()
        {
            var aviokompanija1 = new Aviokompanija
            {
                SifraAviokompanije = "ABC",
                Letovi = this.Letovi,
                Obrisano = false
            };
            var aviokompanija2 = new Aviokompanija
            {
                SifraAviokompanije = "ACC",
                Letovi = this.Letovi,
                Obrisano = false
            };
            Aviokompanije.Add(aviokompanija1);
            Aviokompanije.Add(aviokompanija2);
        }

        internal void UcitajSveLetove()
        {
            var let1 = new Let
            {
                SifraLeta = "123",
                Polaziste = "BEG",
                Destinacija = "LAX",
                VremePolaska = new DateTime(2018, 10, 18, 14, 00, 00),
                VremeDolaska = new DateTime(2018, 10, 18, 16, 00, 00),
                CenaKarte = 13000,
                Obrisano = false
            };

            var let2 = new Let
            {
                SifraLeta = "124",
                Polaziste = "BEG",
                Destinacija = "JFK",
                VremePolaska = new DateTime(2018, 10, 18, 15, 30, 00),
                VremeDolaska = new DateTime(2018, 10, 18, 18, 00, 00),
                CenaKarte = 23000,
                Obrisano = false
            };

            Letovi.Add(let1);
            Letovi.Add(let2);
        }*/

        private static Aplikacija _instanca = null;

        public static Aplikacija Instance
        {
            get
            {
                if(_instanca == null)
                {
                    _instanca = new Aplikacija();
                }
                return _instanca;
            }
        }
        /*
        public void UcitatiSveAerodrome()
        {
            XmlReader reader = XmlReader.Create("..//..//Data//Aerodromi.xml");

            while (reader.Read())
            {
                if(reader.NodeType.Equals(XmlNodeType.Element) && reader.Name.Equals("aerodrom"))
                {
                    var aerodrom = new Aerodrom
                    {
                        Sifra = reader.GetAttribute("sifra"),
                        Naziv = reader.GetAttribute("naziv"),
                        Grad = reader.GetAttribute("grad")
                    };
                    Aerodromi.Add(aerodrom);
                }
            }
            reader.Close();
        }

        public void SacuvajSveAerodrome()
        {
            XmlWriter writer = XmlWriter.Create("..//..//Data//Aerodromi.xml");
            writer.WriteStartElement("aerodromi");
            foreach (var aerodrom in Aerodromi)
            {
                writer.WriteStartElement("aerodrom");
                writer.WriteAttributeString("sifra", aerodrom.Sifra);
                writer.WriteAttributeString("naziv", aerodrom.Naziv);
                writer.WriteAttributeString("grad", aerodrom.Grad);
                writer.WriteEndElement();

            }

            writer.WriteEndDocument();
            writer.Close();
        }

        public void UcitajSveKorisnike()
        {
            Korisnik korisnik1 = new Korisnik();
            korisnik1.Ime = "Pera";
            korisnik1.Prezime = "Peric";
            korisnik1.Email = "peraperic@gmail.com";
            korisnik1.AdresaStanovanja = "Bulevar Oslobodjenja 15";
            korisnik1.Pol = EPol.M;
            korisnik1.KorisnickoIme = "pera";
            korisnik1.Lozinka = "pera";
            korisnik1.TipKorisnika = ETipKorisnika.ADMINISTRATOR;
            korisnik1.Obrisano = false;

            var korisnik2 = new Korisnik("Zika", "Zikic", "zikazikic@gmail.com", "Pavla Vujisica 11", EPol.M, "zika", "zika", ETipKorisnika.NEREGISTROVAN, false);
            var korisnik3 = new Korisnik
            {
                Ime = "Mika",
                Prezime = "Mikic",
                Email = "mikamikic@gmail.com",
                AdresaStanovanja = "Bulevar Kralja Petra 2",
                Pol = EPol.M,
                KorisnickoIme = "mika",
                Lozinka = "mika",
                TipKorisnika = ETipKorisnika.PUTNIK,
                Obrisano = false
            };

            Korisnici.Add(korisnik1);
            Korisnici.Add(korisnik2);
            Korisnici.Add(korisnik3);
        }*/
    }
}
