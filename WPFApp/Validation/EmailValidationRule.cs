﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WPFApp.Validation
{
    class EmailValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value.ToString().Contains("@") && value.ToString().Contains(".com"))
            {
                return new ValidationResult(true, "Email format is correct!");
            }
            return new ValidationResult(false, "Email format is wrong!");
        }
    }
}
