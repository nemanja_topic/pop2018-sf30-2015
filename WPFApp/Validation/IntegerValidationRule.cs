﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WPFApp.Validation
{
    class IntegerValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            Regex regex = new Regex(@"^\d$");
            Match match = regex.Match(value.ToString());
            if (match.Success)
            {
                return new ValidationResult(true, "Accepted!");
            }
            return new ValidationResult(false, "Only accepts integers!");
        }
    }
}
