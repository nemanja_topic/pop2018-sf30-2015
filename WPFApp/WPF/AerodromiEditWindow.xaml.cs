﻿using ConsoleApp.Model;
using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPFApp.WPF
{
    /// <summary>
    /// Interaction logic for AerodromiEditWindow.xaml
    /// </summary>
    public partial class AerodromiEditWindow : Window
    {

        public enum Option
        {
            ADD,
            EDIT
        }

        Aerodrom aerodrom;
        Option option;

        public AerodromiEditWindow(Aerodrom aerodrom, Option option)
        {
            InitializeComponent();
            this.aerodrom = aerodrom;
            this.option = option;

            this.DataContext = aerodrom;

            if (option.Equals(Option.EDIT))
            {
                TbSifra.IsEnabled = false;
            }
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if(TbSifra.Text.Equals(String.Empty) || TbNaziv.Text.Equals(String.Empty) || TbGrad.Text.Equals(String.Empty))
            {
                MessageBox.Show("Popunite sva polja!");
            }
            else
            {
                if(aerodrom.Sifra.Length != 3)
                {
                    MessageBox.Show("Sifra aerodroma moraju biti 3 znaka!");
                    return;
                }
                if (!option.Equals(Option.EDIT))
                {
                    foreach (Aerodrom a in Aplikacija.Instance.Aerodromi)
                    {
                        if (a.Sifra == aerodrom.Sifra)
                        {
                            MessageBox.Show("Vec postoji aerodrom sa tom sifrom!");
                            return;
                        }
                    }
                }
                this.DialogResult = true;
                if (option.Equals(Option.ADD) && !PostojiAerodrom(aerodrom.Sifra))
                {
                    aerodrom.Dodavanje();
                }
            }
            
        }

        private bool PostojiAerodrom(string sifra)
        {
            foreach(var aerodrom in Aplikacija.Instance.Aerodromi)
            {
                if (aerodrom.Sifra.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }

        private void BtnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            //if (MessageBox.Show("Da li ste sigurni da želite otkazati izmene?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
            //{
            //    this.Close();
            //}
        }
    }
}
