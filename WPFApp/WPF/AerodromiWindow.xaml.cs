﻿using ConsoleApp.Model;
using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPFApp.WPF
{
    /// <summary>
    /// Interaction logic for AerodromiWindow.xaml
    /// </summary>
    public partial class AerodromiWindow : Window
    {
        ICollectionView view;
        public AerodromiWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Aerodromi);
            DgAerodromi.ItemsSource = view;
            DgAerodromi.IsSynchronizedWithCurrentItem = true;
            DgAerodromi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            //DgAerodromi.Items.Refresh();
            //Reload();
        }


        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom aerodrom = (Aerodrom)DgAerodromi.SelectedItem;
            if (SelektovanAerodrom(aerodrom))
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var indeks = IndeksSelektovanogAerodroma(aerodrom.Sifra);
                    aerodrom.Obrisano = true;
                    aerodrom.Izmena();
                    Aplikacija.Instance.Aerodromi[indeks].Obrisano = true;
                    view.Refresh();
                }
            }
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom aerodrom = (Aerodrom)DgAerodromi.SelectedItem;
            if (SelektovanAerodrom(aerodrom))
            {
                Aerodrom stari = aerodrom.Clone() as Aerodrom;
                AerodromiEditWindow aew = new AerodromiEditWindow(aerodrom, AerodromiEditWindow.Option.EDIT);
                if(aew.ShowDialog() != true)
                {
                    int indeks = IndeksSelektovanogAerodroma(aerodrom.Sifra);
                    Aplikacija.Instance.Aerodromi[indeks] = stari;
                }
                else
                {
                    aerodrom.Izmena();
                }
            }
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AerodromiEditWindow aew = new AerodromiEditWindow(new Aerodrom(), AerodromiEditWindow.Option.ADD);
            if (aew.ShowDialog() == true)
            {

            }
        }

        private int IndeksSelektovanogAerodroma(String sifra)
        {
            var indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Aerodromi.Count; i++)
            {
                if (Aplikacija.Instance.Aerodromi[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanAerodrom(Aerodrom aerodrom)
        {
            if (aerodrom == null)
            {
                MessageBox.Show("Nije selektovan aerodrom");
                return false;
            }
            return true;
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void DgAerodromi_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.Column.Header.ToString() == "Id" || e.Column.Header.ToString() == "Obrisano")
            {
                e.Cancel = true;
            }
        }
    }
}
