﻿using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFApp.Model;

namespace WPFApp.WPF
{
    /// <summary>
    /// Interaction logic for AviokompanijeEditWindow.xaml
    /// </summary>
    public partial class AviokompanijeEditWindow : Window
    {
        Aviokompanija aviokompanija;

        public AviokompanijeEditWindow(Aviokompanija aviokompanija)
        {
            InitializeComponent();
            this.aviokompanija = aviokompanija;
            this.DataContext = aviokompanija;
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if (TbSifra.Equals(String.Empty))
            {
                MessageBox.Show("Popunite polje!");
                return;
            }
            foreach (Aviokompanija a in Aplikacija.Instance.Aviokompanije)
            {
                if (a.SifraAviokompanije.Equals(aviokompanija.SifraAviokompanije))
                {
                    MessageBox.Show("Vec postoji aviokompanija sa tom sifrom!");
                    return;
                }
            }
            if(TbSifra.Text.Length != 3)
            {
                MessageBox.Show("Sifra aviokompanije moraju biti 3 znaka!");
                return;
            }
            this.DialogResult = true;
            if (!PostojiAerodrom(aviokompanija.Id))
            {
                aviokompanija.Dodavanje();
            }
        }

        private bool PostojiAerodrom(int id)
        {
            foreach (var aviokompanija in Aplikacija.Instance.Aviokompanije)
            {
                if (aviokompanija.Id == id)
                {
                    return true;
                }
            }
            return false;
        }

        private void BtnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
