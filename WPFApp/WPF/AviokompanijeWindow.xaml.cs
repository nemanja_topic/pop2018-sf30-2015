﻿using ConsoleApp.Model;
using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFApp.Model;

namespace WPFApp.WPF
{
    /// <summary>
    /// Interaction logic for AviokompanijeWindow.xaml
    /// </summary>
    public partial class AviokompanijeWindow : Window
    {
        ICollectionView view;
        public AviokompanijeWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Aviokompanije);
            view.Filter = CustomFilter;
            DgAviokompanije.ItemsSource = view;
            DgAviokompanije.IsSynchronizedWithCurrentItem = true;
            DgAviokompanije.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private bool CustomFilter(object obj)
        {
            Aviokompanija aviokompanija = obj as Aviokompanija;//kastovanje na drugi nacin
            if (TxtSearch.Text.Equals(String.Empty))
            {
                return !aviokompanija.Obrisano;
            }
            else
            {
                return !aviokompanija.Obrisano && aviokompanija.SifraAviokompanije.Contains(TxtSearch.Text); //pretraga
            }
        }

        private void BtnLetovi_Click(object sender, RoutedEventArgs e)
        {
            Aviokompanija aviokompanija = (Aviokompanija)DgAviokompanije.SelectedItem;
            LetoviWindow lw = new LetoviWindow(aviokompanija,LetoviWindow.Option.AVIOKOMPANIJA);
            lw.Show();
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AviokompanijeEditWindow aew = new AviokompanijeEditWindow(new Aviokompanija());
            if (aew.ShowDialog() == true)
            {
            }
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Aviokompanija aviokompanija = (Aviokompanija)DgAviokompanije.SelectedItem;
            if (SelektovanaAviokompanija(aviokompanija))
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var indeks = IndeksSelektovaneAviokompanije(aviokompanija.SifraAviokompanije);
                    aviokompanija.Obrisano = true;
                    aviokompanija.Izmena();
                    Aplikacija.Instance.Aviokompanije[indeks].Obrisano = true;
                    view.Refresh();
                }
            }
        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private int IndeksSelektovaneAviokompanije(String sifra)
        {
            var indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Aviokompanije.Count; i++)
            {
                if (Aplikacija.Instance.Aviokompanije[i].SifraAviokompanije.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanaAviokompanija(Aviokompanija aviokompanija)
        {
            if (aviokompanija == null)
            {
                MessageBox.Show("Nije selektovana aviokompanija");
                return false;
            }
            return true;
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void DgAviokompanije_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.Column.Header.ToString() == "Id" || e.Column.Header.ToString() == "Obrisano" || e.Column.Header.ToString() == "Letovi")
            {
                e.Cancel = true;
            }
        }
    }
}
