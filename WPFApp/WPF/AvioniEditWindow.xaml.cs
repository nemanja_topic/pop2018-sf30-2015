﻿using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFApp.Model;

namespace WPFApp.WPF
{
    /// <summary>
    /// Interaction logic for AvioniEditWindow.xaml
    /// </summary>
    public partial class AvioniEditWindow : Window
    {
        public enum Option
        {
            ADD,
            EDIT
        }

        Avion avion;
        Option option;

        public AvioniEditWindow(Avion avion, Option option)
        {
            InitializeComponent();
            this.avion = avion;
            this.option = option;

            CbSifraAviokompanije.ItemsSource = Aplikacija.Instance.Aviokompanije;
            CbSifraLeta.ItemsSource = Aplikacija.Instance.Letovi;

            string biznisKolona = TxtBiznisKolona.Text;
            string biznisRed = TxtBiznisRedova.Text;
            string ekonomskaKolona = TxtEkonomskaKolone.Text;
            string ekonomskaRed = TxtEkonomskaRedovi.Text;

            this.DataContext = avion;
            TxtBiznisKolona.DataContext = biznisKolona;
            TxtBiznisRedova.DataContext = biznisRed;
            TxtEkonomskaKolone.DataContext = ekonomskaKolona;
            TxtEkonomskaRedovi.DataContext = ekonomskaRed;



            if (option.Equals(Option.EDIT))
            {
                TxtBiznisKolona.IsEnabled = false;
                TxtBiznisRedova.IsEnabled = false;
                TxtEkonomskaKolone.IsEnabled = false;
                TxtEkonomskaRedovi.IsEnabled = false;
            }
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if(avion.Pilot == null)
            {
                avion.Pilot = String.Empty;
            }
            if(avion.Pilot.Equals(String.Empty) || CbSifraLeta.SelectedItem == null || CbSifraAviokompanije.SelectedItem == null)
            {
                MessageBox.Show("Popunite sva polja!");
                return;
            }
                this.DialogResult = true;
            if (option.Equals(Option.ADD))
            {
                if(TxtBiznisKolona.Equals(String.Empty) || TxtBiznisRedova.Equals(String.Empty)
                || TxtEkonomskaKolone.Equals(String.Empty) || TxtEkonomskaRedovi.Equals(String.Empty))
                {
                    MessageBox.Show("Popunite sva polja!");
                    return;
                }
                string biznisKolona = TxtBiznisKolona.Text;
                string biznisRed = TxtBiznisRedova.Text;
                string ekonomskaKolona = TxtEkonomskaKolone.Text;
                string ekonomskaRed = TxtEkonomskaRedovi.Text;

                bool one = Int32.TryParse(biznisKolona, out int biznisKolonaInt);
                bool two = Int32.TryParse(biznisRed, out int biznisRedInt);
                bool three = Int32.TryParse(ekonomskaKolona, out int ekonomskaKolonaInt);
                bool four = Int32.TryParse(ekonomskaRed, out int ekonomskaRedInt);

                if (!one || !two || !three || !four)
                {
                    MessageBox.Show("Kolone i redovi mogu biti samo celi brojevi!");
                    return;
                }
                if (biznisKolonaInt > biznisRedInt || ekonomskaKolonaInt > ekonomskaRedInt)
                {
                    MessageBox.Show("Broj kolona ne moze biti veci od broja redova!");
                    return;
                }

                avion.Dodavanje();
                Avion aavion = new Avion();
                aavion = Aplikacija.Instance.Avioni.Last();

                for (int i = 1; i <= biznisKolonaInt; i++)
                {
                    for (int j = 1; j <= biznisRedInt; j++)
                    {
                        Sediste sediste = new Sediste()
                        {
                            Kolona = i,
                            Red = j,
                            Biznis = true,
                            Zauzeto = false,
                            AvionId = aavion.Id
                        };
                        sediste.Dodavanje();
                    }
                }

                for (int i = 1; i <= ekonomskaKolonaInt; i++)
                {
                    for (int j = 1; j <= ekonomskaRedInt; j++)
                    {
                        Sediste sediste = new Sediste()
                        {
                            Kolona = i,
                            Red = j,
                            Biznis = false,
                            Zauzeto = false,
                            AvionId = aavion.Id
                        };
                        sediste.Dodavanje();
                    }
                }
            }
        }

        private bool PostojiAvion(int id)
        {
            foreach (var avion in Aplikacija.Instance.Avioni)
            {
                if (avion.Id == id)
                {
                    return true;
                }
            }
            return false;
        }

        private void CheckInt(string kolona, string red)
        {
            int kolonaInt;
            int redInt;

            if (!Int32.TryParse(kolona, out kolonaInt) &&
                !Int32.TryParse(red, out redInt))
            {
                kolonaInt = -1;
                redInt = -1;
            }            
        }

        private void BtnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

    }
}
