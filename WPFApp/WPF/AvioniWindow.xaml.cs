﻿using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFApp.Model;

namespace WPFApp.WPF
{
    /// <summary>
    /// Interaction logic for AvioniWindow.xaml
    /// </summary>
    public partial class AvioniWindow : Window
    {
        ICollectionView view;
        public AvioniWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Avioni);
            view.Filter = CustomFilter;
            DgAvioni.ItemsSource = view;
            DgAvioni.IsSynchronizedWithCurrentItem = true;
            DgAvioni.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private bool CustomFilter(object obj)
        {
            Avion avion = obj as Avion;//kastovanje na drugi nacin
            if (TxtSearch.Text.Equals(String.Empty))
            {
                return !avion.Obrisano;
            }
            else
            {
                return !avion.Obrisano && avion.Pilot.Contains(TxtSearch.Text); //pretraga
            }
        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Avion avion = (Avion)DgAvioni.SelectedItem;
            if (SelektovanAvion(avion))
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var indeks = IndeksSelektovanogAviona(avion.Id);
                    avion.Obrisano = true;
                    avion.Izmena();
                    Aplikacija.Instance.Avioni[indeks].Obrisano = true;
                    view.Refresh();
                }
            }
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Avion avion = (Avion)DgAvioni.SelectedItem;
            if (SelektovanAvion(avion))
            {
                Avion stari = avion.Clone() as Avion;
                AvioniEditWindow aew = new AvioniEditWindow(avion, AvioniEditWindow.Option.EDIT);
                if (aew.ShowDialog() != true)
                {
                    int indeks = IndeksSelektovanogAviona(avion.Id);
                    Aplikacija.Instance.Avioni[indeks] = stari;
                }
                else
                {
                    avion.Izmena();
                }
            }
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
            {
                AvioniEditWindow aew = new AvioniEditWindow(new Avion(), AvioniEditWindow.Option.ADD);
                if (aew.ShowDialog() == true)
                {
                }
            }

        private int IndeksSelektovanogAviona(int id)
        {
            var indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Avioni.Count; i++)
            {
                if (Aplikacija.Instance.Avioni[i].Id == id)
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanAvion(Avion avion)
        {
            if (avion == null)
            {
                MessageBox.Show("Nije selektovan avion");
                return false;
            }
            return true;
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void BtnSedista_Click(object sender, RoutedEventArgs e)
        {
            Avion avion = (Avion)DgAvioni.SelectedItem;
            SedistaWindow sw = new SedistaWindow(avion);
            sw.Show();
        }

        private void DgAvioni_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.Column.Header.ToString() == "Sedista" || e.Column.Header.ToString() == "Let" || e.Column.Header.ToString() == "Aviokompanija" || e.Column.Header.ToString() == "Obrisano")
            {
                e.Cancel = true;
            }
        }
    }
}
