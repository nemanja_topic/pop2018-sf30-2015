﻿using ConsoleApp.Model;
using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFApp.Model;

namespace WPFApp.WPF
{
    /// <summary>
    /// Interaction logic for KarteEditWindow.xaml
    /// </summary>
    public partial class KarteEditWindow : Window
    {
        public enum Option
        {
            ADD,
            EDIT,
            BUY
        }

        Karta karta;
        Option option;
        Let let;

        public KarteEditWindow(Karta karta, Let let, Option option)
        {
            InitializeComponent();
            this.karta = karta;
            this.let = let;
            this.option = option;

            CbSifraLeta.ItemsSource = Aplikacija.Instance.Letovi;

            TbCenaKarte.IsEnabled = false;

            if (!option.Equals(Option.EDIT))
            {
                Random random = new Random();
                karta.Kapija = random.Next(1, 100);
            }
            if (option.Equals(Option.BUY))
            {
                karta.Let = let;
                CbSifraLeta.SelectedItem = karta.Let;
                CbSifraLeta.IsEnabled = false;
            }
            if (Aplikacija.Instance.UlogovaniKorisnik != null)
            {
                if (Aplikacija.Instance.UlogovaniKorisnik.TipKorisnika == ETipKorisnika.PUTNIK)
            {
                    TbKorisnickoIme.IsEnabled = false;
                    karta.NazivPutnika = Aplikacija.Instance.UlogovaniKorisnik.KorisnickoIme;
            }
                if (Aplikacija.Instance.UlogovaniKorisnik.TipKorisnika == ETipKorisnika.ADMINISTRATOR && option.Equals(Option.ADD))
                {
                    TbKorisnickoIme.IsEnabled = false;
                    karta.NazivPutnika = Aplikacija.Instance.UlogovaniKorisnik.KorisnickoIme;
                }
                if (Aplikacija.Instance.UlogovaniKorisnik.TipKorisnika == ETipKorisnika.ADMINISTRATOR && option.Equals(Option.EDIT))
            {
                    TbKorisnickoIme.IsEnabled = false;
            }
            }
            karta.UkupnaCena = let.CenaKarte;
            this.DataContext = karta;
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if (CbSifraLeta.SelectedItem == null || String.IsNullOrEmpty(TbKorisnickoIme.Text) || CbSediste.SelectedItem == null)
            {
                MessageBox.Show("Popunite sva polja!");
                return;
            }
            karta.Sediste.Zauzeto = true;
            karta.IdSedista = karta.Sediste.Id;
            karta.IdLeta = karta.Let.Id;
            this.DialogResult = true;
            if (option.Equals(Option.ADD) && !PostojiKarta(karta.Id))
            {
                karta.Dodavanje();
            }
            if (option.Equals(Option.BUY) && !PostojiKarta(karta.Id))
            {
                karta.Dodavanje();
            }
            karta.Sediste.Izmena();
        }

        private bool PostojiKarta(int id)
        {
            foreach (var karta in Aplikacija.Instance.Karte)
            {
                if (karta.Id == id)
                {
                    return true;
                }
            }
            return false;
        }

        private void BtnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ChangeCbSedista()
        {
            ObservableCollection<Sediste> sedista = new ObservableCollection<Sediste>();
            if (ChBiznisKlasa.IsChecked == true)
            {
                sedista.Clear();
                karta.UkupnaCena = karta.Let.CenaKarte * 1.5;
                foreach (Avion a in Aplikacija.Instance.Avioni)
                {
                    if (a.IdLeta == karta.Let.Id)
                    {
                        foreach (Sediste s in Aplikacija.Instance.Sedista)
                        {
                            if (s.AvionId == a.Id)
                            {
                                if (s.Biznis == true && s.Zauzeto == false)
                                {
                                    sedista.Add(s);
                                }
                            }
                        }
                    }
                }
                CbSediste.ItemsSource = sedista;
            }
            else
            {
                sedista.Clear();
                karta.UkupnaCena = karta.Let.CenaKarte;
                foreach (Avion a in Aplikacija.Instance.Avioni)
                {
                    if (a.IdLeta == karta.Let.Id)
                    {
                        foreach (Sediste s in Aplikacija.Instance.Sedista)
                        {
                            if (s.AvionId == a.Id)
                            {
                                if (s.Biznis == false && s.Zauzeto == false)
                                {
                                    sedista.Add(s);
                                }
                            }
                        }
                    }
                }
                CbSediste.ItemsSource = sedista;
            }
        }

        private void ChBiznisKlasa_Click(object sender, RoutedEventArgs e)
        {
            ChangeCbSedista();
        }

        private void CbSifraLeta_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            karta.Let = (Let)CbSifraLeta.SelectedItem;
            ChangeCbSedista();
        }
    }
}
