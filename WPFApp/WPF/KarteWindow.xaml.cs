﻿using ConsoleApp.Model;
using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFApp.Model;

namespace WPFApp.WPF
{
    /// <summary>
    /// Interaction logic for KarteWindow.xaml
    /// </summary>
    public partial class KarteWindow : Window
    {
        ICollectionView view;
        public KarteWindow()
        {
            InitializeComponent();

            if (Aplikacija.Instance.UlogovaniKorisnik.TipKorisnika == ETipKorisnika.PUTNIK)
            {
                view = CollectionViewSource.GetDefaultView(getKarteByKorisnik(Aplikacija.Instance.Karte));
            }
            else
            {
                view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Karte);
            }

            view.Filter = CustomFilter;
            DgKarte.ItemsSource = view;
            DgKarte.IsSynchronizedWithCurrentItem = true;
            DgKarte.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        public ObservableCollection<Karta> getKarteByKorisnik(ObservableCollection<Karta> karte)
        {
            ObservableCollection<Karta> kartee = new ObservableCollection<Karta>();
            foreach (Karta k in karte)
            {
                if (k.NazivPutnika == Aplikacija.Instance.UlogovaniKorisnik.KorisnickoIme)
                {
                    kartee.Add(k);
                }
            }
            return kartee;
        }

        private bool CustomFilter(object obj)
        {
            Karta karta = obj as Karta;//kastovanje na drugi nacin
            if (TxtSearch.Text.Equals(String.Empty))
            {
                return !karta.Obrisano;
            }
            else
            {
                return !karta.Obrisano && karta.NazivPutnika.Contains(TxtSearch.Text); //pretraga
            }
        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Karta karta = (Karta)DgKarte.SelectedItem;
            if (SelektovanaKarta(karta))
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var indeks = IndeksSelektovaneKarte(karta.Id);
                    karta.Obrisano = true;
                    karta.Izmena();
                    Aplikacija.Instance.Karte[indeks].Obrisano = true;
                    view.Refresh();
                }
            }
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Karta karta = (Karta)DgKarte.SelectedItem;
            if (SelektovanaKarta(karta))
            {
                Karta stara = karta.Clone() as Karta;
                Let lett = new Let();
                foreach(Let let in Aplikacija.Instance.Letovi)
                {
                    if(karta.IdLeta == let.Id)
                    {
                        lett = let;
                    }
                }
                karta.Sediste.Zauzeto = false;
                karta.Sediste.Izmena();
                KarteEditWindow kew = new KarteEditWindow(karta, lett, KarteEditWindow.Option.EDIT);
                if (kew.ShowDialog() != true)
                {
                    int indeks = IndeksSelektovaneKarte(karta.Id);
                    stara.Sediste.Zauzeto = true;
                    stara.Sediste.Izmena();
                    Aplikacija.Instance.Karte[indeks] = stara;
                }
                else
                {
                    karta.Izmena();
                }
            }
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            KarteEditWindow kew = new KarteEditWindow(new Karta(), new Let(), KarteEditWindow.Option.ADD);
            if (kew.ShowDialog() == true)
            {
            }
        }

        private int IndeksSelektovaneKarte(int id)
        {
            var indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Karte.Count; i++)
            {
                if (Aplikacija.Instance.Karte[i].Id == id)
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanaKarta(Karta karta)
        {
            if (karta == null)
            {
                MessageBox.Show("Nije selektovana karta");
                return false;
            }
            return true;
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void DgKarte_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.Column.Header.ToString() == "Let" || e.Column.Header.ToString() == "Sediste" || e.Column.Header.ToString() == "Obrisano")
            {
                e.Cancel = true;
            }
        }
    }
}
