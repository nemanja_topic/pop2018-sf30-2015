﻿using ConsoleApp.Model;
using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPFApp.WPF
{
    /// <summary>
    /// Interaction logic for KorisniciEditWindow.xaml
    /// </summary>
    public partial class KorisniciEditWindow : Window
    {
        public enum Option
        {
            ADD,
            EDIT,
            REGISTER
        }

        public Korisnik korisnik;
        public Option option;

        public KorisniciEditWindow(Korisnik korisnik, Option option)
        {
            InitializeComponent();
            this.korisnik = korisnik;
            this.option = option;

            this.DataContext = korisnik;
            CbPol.ItemsSource = new ObservableCollection<EPol> { EPol.M, EPol.Z };
            if (option.Equals(Option.EDIT))
            {
                TxtKorisnickoIme.IsEnabled = false;
            }
            if (option.Equals(Option.REGISTER))
            {
                CbTipKorisnika.ItemsSource = new ObservableCollection<ETipKorisnika> { ETipKorisnika.PUTNIK };
            }
            else
            {
                CbTipKorisnika.ItemsSource = new ObservableCollection<ETipKorisnika> { ETipKorisnika.ADMINISTRATOR, ETipKorisnika.PUTNIK };
            }
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if (!System.Windows.Controls.Validation.GetHasError(TxtEmail))
            {
                this.DialogResult = true;
                if (option.Equals(Option.ADD ) && !PostojiKorisnik(korisnik.KorisnickoIme))
                {
                    korisnik.Dodavanje();
                }
                if (option.Equals(Option.REGISTER) && !PostojiKorisnik(korisnik.KorisnickoIme))
                {
                    korisnik.Dodavanje();
                }
            }
            if(String.IsNullOrEmpty(TxtIme.Text) || String.IsNullOrEmpty(TxtPrezime.Text) ||
                String.IsNullOrEmpty(TxtEmail.Text) || String.IsNullOrEmpty(TxtAdresa.Text) ||
                String.IsNullOrEmpty(TxtKorisnickoIme.Text) || String.IsNullOrEmpty(TxtPassword.Text) ||
                CbPol.SelectedItem == null || CbTipKorisnika == null)
            {
                MessageBox.Show("Popunite sva polja!");
            }
            if (!option.Equals(Option.EDIT))
            {
                foreach(Korisnik k in Aplikacija.Instance.Korisnici)
                {
                    if (k.KorisnickoIme.Equals(korisnik.KorisnickoIme) || k.Email.Equals(korisnik.Email))
                    {
                        MessageBox.Show("Postoji korisnik sa tim korisnickim imenom ili email adresom!");
                        return;
                    }
                }
            }
        }

        private bool PostojiKorisnik(string korisnickoIme)
        {
            foreach (var korisnik in Aplikacija.Instance.Korisnici)
            {
                if (korisnik.KorisnickoIme.Equals(korisnickoIme))
                {
                    return true;
                }
            }
            return false;
        }

        private void BtnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
