﻿using ConsoleApp.Model;
using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPFApp.WPF
{
    /// <summary>
    /// Interaction logic for KorisniciWindow.xaml
    /// </summary>
    public partial class KorisniciWindow : Window
    {
        ICollectionView view;
        public KorisniciWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Korisnici);
            view.Filter = CustomFilter;
            DgKorisnici.ItemsSource = view;
            DgKorisnici.IsSynchronizedWithCurrentItem = true;
            DgKorisnici.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private bool CustomFilter(object obj)
        {
            Korisnik korisnik = obj as Korisnik;

            if (!TbIme.Text.Equals(String.Empty) || !TbPrezime.Text.Equals(String.Empty) || !TbKorisnickoIme.Text.Equals(String.Empty) || CbTipKorisnika.SelectedItem != null)
            {
                if (!TbIme.Text.Equals(String.Empty) && !TbPrezime.Text.Equals(String.Empty) && !TbKorisnickoIme.Text.Equals(String.Empty) && CbTipKorisnika.SelectedItem != null)
                {
                    return !korisnik.Obrisano && korisnik.Ime.Contains(TbIme.Text) && korisnik.Prezime.Contains(TbPrezime.Text) && korisnik.KorisnickoIme.Contains(TbKorisnickoIme.Text) && korisnik.TipKorisnika.ToString() == CbTipKorisnika.SelectedValue.ToString();
                }
                if(!TbIme.Text.Equals(String.Empty) && !TbPrezime.Text.Equals(String.Empty) && !TbKorisnickoIme.Text.Equals(String.Empty))
                {
                    return !korisnik.Obrisano && korisnik.Ime.Contains(TbIme.Text) && korisnik.Prezime.Contains(TbPrezime.Text) && korisnik.KorisnickoIme.Contains(TbKorisnickoIme.Text);
                }
                if(!TbIme.Text.Equals(String.Empty) && !TbPrezime.Text.Contains(String.Empty))
                {
                    return !korisnik.Obrisano && korisnik.Ime.Contains(TbIme.Text) && korisnik.Prezime.Contains(TbPrezime.Text);
                }
                if (!TbIme.Text.Equals(String.Empty))
                {
                    return !korisnik.Obrisano && korisnik.Ime.Contains(TbIme.Text);
                }
                if (!TbPrezime.Text.Equals(String.Empty))
                {
                    return !korisnik.Obrisano && korisnik.Prezime.Contains(TbPrezime.Text);
                }
                if (!TbKorisnickoIme.Text.Equals(String.Empty))
                {
                    return !korisnik.Obrisano && korisnik.KorisnickoIme.Contains(TbKorisnickoIme.Text);
                }
                if (CbTipKorisnika.SelectedItem != null)
                {
                    return !korisnik.Obrisano && korisnik.TipKorisnika.ToString().Equals(CbTipKorisnika.SelectedItem.ToString());
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return !korisnik.Obrisano;
            }

        }

        /* private bool CustomFilter(object obj)
         {
             Korisnik korisnik = obj as Korisnik;
             if (!TbIme.Text.Equals(String.Empty))
             {
                 TbPrezime.Clear();
                 TbKorisnickoIme.Clear();
                 CbTipKorisnika.SelectedItem = null;
                 return !korisnik.Obrisano && korisnik.Ime.Equals(TbIme.Text);
             }
             if (!TbPrezime.Text.Equals(String.Empty))
             {
                 TbIme.Clear();
                 TbKorisnickoIme.Clear();
                 CbTipKorisnika.SelectedItem = null;
                 return !korisnik.Obrisano && korisnik.Prezime.Equals(TbPrezime.Text);

             }
             if (!TbKorisnickoIme.Text.Equals(String.Empty))
             {
                 TbPrezime.Clear();
                 TbIme.Clear();
                 CbTipKorisnika.SelectedItem = null;
                 return !korisnik.Obrisano && korisnik.KorisnickoIme.Equals(TbKorisnickoIme.Text);

             }
             if (CbTipKorisnika.SelectedItem != null)
             {
                 TbPrezime.Clear();
                 TbKorisnickoIme.Clear();
                 TbIme.Clear();
                 Korisnik korisnikk = (Korisnik)CbTipKorisnika.SelectedItem;
                 return !korisnik.Obrisano && korisnik.TipKorisnika == korisnikk.TipKorisnika;

             }
             else
             {
                 return !korisnik.Obrisano;
             }
         }*/

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            KorisniciEditWindow kew = new KorisniciEditWindow(new Korisnik(), KorisniciEditWindow.Option.ADD);
            if (kew.ShowDialog() == true)
            {

            }
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = (Korisnik)DgKorisnici.SelectedItem;
            if (SelektovanKorisnik(korisnik))
            {
                Korisnik stari = korisnik.Clone() as Korisnik;
                KorisniciEditWindow kew = new KorisniciEditWindow(korisnik, KorisniciEditWindow.Option.EDIT);
                if (kew.ShowDialog() != true)
                {
                    int indeks = IndeksSelektovanogKorisnika(korisnik.KorisnickoIme);
                    Aplikacija.Instance.Korisnici[indeks] = stari;
                }
                else
                {
                    korisnik.Izmena();
                }
            }
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = (Korisnik)DgKorisnici.SelectedItem;
            if (SelektovanKorisnik(korisnik))
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var indeks = IndeksSelektovanogKorisnika(korisnik.KorisnickoIme);
                    korisnik.Obrisano = true;
                    korisnik.Izmena();
                    Aplikacija.Instance.Korisnici[indeks].Obrisano = true;
                    view.Refresh();
                }
            }
        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private int IndeksSelektovanogKorisnika(String korisnickoIme)
        {
            var indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Korisnici.Count; i++)
            {
                if (Aplikacija.Instance.Korisnici[i].KorisnickoIme.Equals(korisnickoIme))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanKorisnik(Korisnik korisnik)
        {
            if (korisnik == null)
            {
                MessageBox.Show("Nije selektovan korisnik");
                return false;
            }
            return true;
        }

        private void DgKorisnici_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.Column.Header.ToString() == "Id" || e.Column.Header.ToString() == "Obrisano")
            {
                e.Cancel = true;
            }
        }

        private void TbPrezime_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void TbKorisnickoIme_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void CbTipKorisnika_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            view.Refresh();
        }

        private void TbIme_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}
