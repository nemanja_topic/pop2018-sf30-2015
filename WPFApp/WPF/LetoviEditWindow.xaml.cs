﻿using ConsoleApp.Model;
using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPFApp.WPF
{
    /// <summary>
    /// Interaction logic for LetoviEditWindow.xaml
    /// </summary>
    public partial class LetoviEditWindow : Window
    {
        public enum Option
        {
            ADD,
            EDIT
        }

        public Let let;
        public Option option;

        public LetoviEditWindow(Let let, Option option)
        {
            InitializeComponent();
            this.let = let;
            this.option = option;

            this.DataContext = let;
            CbAviokompanije.ItemsSource = Aplikacija.Instance.Aviokompanije;
            CbPolaziste.ItemsSource = Aplikacija.Instance.Aerodromi.Select(a => a.Sifra);
            CbDestinacija.ItemsSource = Aplikacija.Instance.Aerodromi.Select(a => a.Sifra);
            if (option.Equals(Option.EDIT))
            {
                TxtSifra.IsEnabled = false;
            }
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(TxtSifra.Text) || String.IsNullOrEmpty(TxtCena.Text) ||
                CbAviokompanije.SelectedItem == null || CbDestinacija.SelectedItem == null
                || CbPolaziste.SelectedItem == null || DtpVremeDolaska.Value == null
                || DtpVremePolaska.Value == null)
            {
                MessageBox.Show("Popunite sva polja!");
                return;
            }
            if (!option.Equals(Option.EDIT))
            {
                foreach (Let l in Aplikacija.Instance.Letovi)
                {
                    if(l.SifraLeta == let.SifraLeta)
                    {
                        MessageBox.Show("Vec postoji let sa tom sifrom!");
                        return;
                    }
                }
            }
            this.DialogResult = true;
            if (option.Equals(Option.ADD) && !PostojiLet(let.SifraLeta))
            {
                let.Dodavanje();
            }
        }

        private bool PostojiLet(string sifra)
        {
            foreach (var let in Aplikacija.Instance.Letovi)
            {
                if (let.SifraLeta.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }

        private void BtnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
