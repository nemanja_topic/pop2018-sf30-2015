﻿using ConsoleApp.Model;
using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFApp.Model;

namespace WPFApp.WPF
{
    /// <summary>
    /// Interaction logic for LetoviWindow.xaml
    /// </summary>
    public partial class LetoviWindow : Window
    {
        public enum Option
        {
            PUTNIK,
            AVIOKOMPANIJA,
            ADMINISTRATOR
        }

        public Option option;
        public Aviokompanija aviokompanija;

        ICollectionView view;
        public LetoviWindow(Aviokompanija aviokompanija, Option option)
        {
            InitializeComponent();

            this.option = option;
            this.aviokompanija = aviokompanija;

            CbAviokompanija.ItemsSource = Aplikacija.Instance.Aviokompanije;
            CbDestinacija.ItemsSource = Aplikacija.Instance.Letovi;
            CbPolaziste.ItemsSource = Aplikacija.Instance.Letovi;

            if (option.Equals(Option.AVIOKOMPANIJA))
            {
                view = CollectionViewSource.GetDefaultView(getLetoviByAviokompanijaId(Aplikacija.Instance.Letovi));
                BtnDodaj.Visibility = Visibility.Hidden;
                BtnIzmeni.Visibility = Visibility.Hidden;
                BtnObrisi.Visibility = Visibility.Hidden;
                BtnKupiteKartu.Visibility = Visibility.Hidden;
            }
            else if (option.Equals(Option.PUTNIK))
            {
                view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Letovi);
                BtnDodaj.Visibility = Visibility.Hidden;
                BtnIzmeni.Visibility = Visibility.Hidden;
                BtnObrisi.Visibility = Visibility.Hidden;
            }
            else
            {
                view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Letovi);
                BtnKupiteKartu.Visibility = Visibility.Hidden;
            }

            //view = CollectionViewSource.GetDefaultView(Aplikacija.Instance.Letovi);
            view.Filter = CustomFilter;
            DgLetovi.ItemsSource = view;
            DgLetovi.IsSynchronizedWithCurrentItem = true;
            DgLetovi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

        }

        public ObservableCollection<Let> getLetoviByAviokompanijaId(ObservableCollection<Let> letovi)
        {
            ObservableCollection<Let> letovii = new ObservableCollection<Let>();
            foreach (Let let in letovi)
            {
                if (let.IdAviokompanije == aviokompanija.Id)
                {
                    letovii.Add(let);
                }
            }
            return letovii;
        }

        private bool CustomFilter(object obj)
        {
            Let let = obj as Let;
            if (!TbSifraLeta.Text.Equals(String.Empty))
            {
                return !let.Obrisano && let.SifraLeta.Equals(TbSifraLeta.Text);
            }
            return !let.Obrisano;
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            LetoviEditWindow lew = new LetoviEditWindow(new Let(), LetoviEditWindow.Option.ADD);
            if (lew.ShowDialog() == true)
            {

            }
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Let let = (Let)DgLetovi.SelectedItem;
            if (SelektovanLet(let))
            {
                Let stari = let.Clone() as Let;
                LetoviEditWindow lew = new LetoviEditWindow(let, LetoviEditWindow.Option.EDIT);
                if (lew.ShowDialog() != true)
                {
                    int indeks = IndeksSelektovanogLeta(let.SifraLeta);
                    Aplikacija.Instance.Letovi[indeks] = stari;
                }
                else
                {
                    let.Izmena();
                }
            }
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Let let = (Let)DgLetovi.SelectedItem;
            if (SelektovanLet(let))
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var indeks = IndeksSelektovanogLeta(let.SifraLeta);
                    let.Obrisano = true;
                    let.Izmena();
                    Aplikacija.Instance.Letovi[indeks].Obrisano = true;
                    view.Refresh();
                }
            }
        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private int IndeksSelektovanogLeta(String sifra)
        {
            var indeks = -1;
            for (int i = 0; i < Aplikacija.Instance.Letovi.Count; i++)
            {
                if (Aplikacija.Instance.Letovi[i].SifraLeta.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanLet(Let let)
        {
            if (let == null)
            {
                MessageBox.Show("Nije selektovan let");
                return false;
            }
            return true;
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void DgLetovi_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.Column.Header.ToString() == "Id" || e.Column.Header.ToString() == "Aviokompanija" || e.Column.Header.ToString() == "Obrisano")
            {
                e.Cancel = true;
            }
        }

        private void CbAviokompanija_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            view.Refresh();
        }

        private void BtnKupiteKartu_Click(object sender, RoutedEventArgs e)
        {
            Let let = (Let)DgLetovi.SelectedItem;
            KarteEditWindow kew = new KarteEditWindow(new Karta(), let, KarteEditWindow.Option.BUY);
            if (kew.ShowDialog() == true)
            {
                MessageBox.Show("Karta uspesno kupljena!");
            }
        }
    }
}
