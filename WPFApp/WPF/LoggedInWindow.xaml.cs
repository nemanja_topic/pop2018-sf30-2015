﻿using ConsoleApp.Model;
using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFApp.Model;

namespace WPFApp.WPF
{
    /// <summary>
    /// Interaction logic for LoggedInWindow.xaml
    /// </summary>
    public partial class LoggedInWindow : Window
    {

        Korisnik korisnik;

        public LoggedInWindow(Korisnik korisnik)
        {
            InitializeComponent();

            this.korisnik = korisnik;

            if (korisnik.TipKorisnika == ETipKorisnika.PUTNIK)
            {
                BtnAerodromi.IsEnabled = false;
                BtnAviokompanije.IsEnabled = false;
                BtnAvioni.IsEnabled = false;
                BtnKorisnici.IsEnabled = false;
            }
            if (korisnik.TipKorisnika == ETipKorisnika.ADMINISTRATOR)
            {

            }

        }

        private void BtnAerodromi_Click(object sender, RoutedEventArgs e)
        {
            AerodromiWindow aw = new AerodromiWindow();
            aw.Show();
        }

        private void BtnLetovi_Click(object sender, RoutedEventArgs e)
        {
            if (Aplikacija.Instance.UlogovaniKorisnik.TipKorisnika == ETipKorisnika.ADMINISTRATOR)
            {
                LetoviWindow lw = new LetoviWindow(new Aviokompanija(), LetoviWindow.Option.ADMINISTRATOR);
                lw.Show();
            }
            else
            {
                LetoviWindow lw = new LetoviWindow(new Aviokompanija(), LetoviWindow.Option.PUTNIK);
                lw.Show();
            }
        }

        private void BtnKorisnici_Click(object sender, RoutedEventArgs e)
        {
            KorisniciWindow kw = new KorisniciWindow();
            kw.Show();
        }

        private void BtnAviokompanije_Click(object sender, RoutedEventArgs e)
        {
            AviokompanijeWindow aw = new AviokompanijeWindow();
            aw.Show();
        }

        private void BtnAvioni_Click(object sender, RoutedEventArgs e)
        {
            AvioniWindow aw = new AvioniWindow();
            aw.Show();
        }

        private void BtnKarte_Click(object sender, RoutedEventArgs e)
        {
            KarteWindow kw = new KarteWindow();
            kw.Show();
        }

        private void BtnOdjava_Click(object sender, RoutedEventArgs e)
        {
            Aplikacija.Instance.UlogovaniKorisnik = null;
            this.Close();
        }
    }
}
