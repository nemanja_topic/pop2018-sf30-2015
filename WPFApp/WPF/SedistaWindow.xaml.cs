﻿using ConsoleApp.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFApp.Model;

namespace WPFApp.WPF
{
    /// <summary>
    /// Interaction logic for SedistaWindow.xaml
    /// </summary>
    public partial class SedistaWindow : Window
    {
        ICollectionView view;
        Avion avion;

        public SedistaWindow(Avion avion)
        {
            InitializeComponent();

            this.avion = avion;

            view = CollectionViewSource.GetDefaultView(getSedistaByAvionId(Aplikacija.Instance.Sedista));
            //view.Filter = CustomFilter;
            DgSedista.ItemsSource = view;
            DgSedista.IsSynchronizedWithCurrentItem = true;
            DgSedista.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        public ObservableCollection<Sediste> getSedistaByAvionId(ObservableCollection<Sediste> sedista)
        {
            ObservableCollection<Sediste> sedistaa = new ObservableCollection<Sediste>();
            foreach (Sediste sediste in sedista)
            {
                if(sediste.AvionId == avion.Id)
                {
                    sedistaa.Add(sediste);
                }
            }
            return sedistaa;
        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void DgSedista_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.Column.Header.ToString() == "Id" || e.Column.Header.ToString() == "Avion" || e.Column.Header.ToString() == "Obrisano")
            {
                e.Cancel = true;
            }
        }
    }
}
